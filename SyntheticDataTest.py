import os
import logging
import re
import pickle
import csv
import pandas as pd

import dateutil
import datetime
from dateutil.parser import parse
from datetime import timedelta
import time


import numpy as np
import math
import random
import networkx as nx
from scipy.stats import ks_2samp


import matplotlib.dates as mdates
import matplotlib.pyplot as plt

class SDT():
    def __init__(self,Activation = None,debug=False):
        self.Debug = debug
        self.DegreeVector = None
        self.GroundTruth = None
        #self.GraphComparison = pd.DataFrame(columns=["Interval", "KS-P-Value"])
        if Activation is not None:
            self.Activation = Activation
            self.setdir()
        #self.Population = 0
        #self.NumberOfEvents = 0
        #self.WindowSize = 0
        #self.Slide = 0
        #self.Dir = ''
    def setdir(self):
        if 'Models' in self.Activation:
            if self.Activation['Models'][1]['Name'] == 'Caveman':
                modelparams = str(self.Activation['Models'][1]['Rewire']) + '-' + str(self.Activation['Models'][2]['Rewire'])
            if self.Activation['Models'][1]['Name'] == 'PA':
                modelparams = str(self.Activation['Models'][1]['Talkative']) + '-' + str(self.Activation['Models'][2]['Talkative'])
            if self.Activation['Models'][1]['Name'] == 'ER':
                modelparams = str(self.Activation['Models'][1]['HyperParams'][1]) + '-' + str(self.Activation['Models'][2]['HyperParams'][1])
            sufix = "Du_" + str(self.Activation['Interval_Duration']['mu']) + \
                    "_" + str(self.Activation['Interval_Duration']['sigma'])
            self.Dir = self.Activation['HomeDir'] + '/P_' + str(self.Activation['Population'])+ '_Model_' + self.Activation['Models'][1]['Name'] + modelparams + "_E_" + str(
                self.Activation['TotalEvents']) + sufix
            if 'cycles' in self.Activation:
                self.Dir = self.Dir + '/Cycle' + str(self.Activation['cycles'])
        else:
            self.Dir = self.Activation['HomeDir'] + '/P_' + str(self.Activation['Population']) \
                       + "E_" + str(self.Activation['TotalEvents']) + 'Dist_' + self.Activation['Distributions'][
                           1] + "DU_" + str(self.Activation['Interval_Duration']['mu']) \
                       + "_" + str(self.Activation['Interval_Duration']['sigma'])

    def getdir(self):
        return self.Dir

    def SetParams(self,Population = None,Events = None,Window = None,Slide = None , homedir = None , sufix = ''):
        if Population is not None:
            self.Population = Population
        if Events is not None:
            self.NumberOfEvents = Events
        if Window is not None:
            self.WindowSize = Window
        if Slide is not None:
            self.Slide = Slide

    def SetParameters(self, Activsation,Population=None, Events=None, Window=None, Slide=None, homedir=None, sufix=''):
        self.Activation = Activsation
        self.Dir = Activsation['HomeDir'] + '/P_' + str(Activsation['Population'])   + "E_" + str(Activsation['TotalEvents']) + "DU_" + str(self.Activation['Interval_Duration'][0]) + "_" + str(self.Activation['Interval_Duration'][1])

    def GetParams(self,paramname = None ):
        ret = []
        if paramname is None or 'Population' in paramname:
            ret.append(self.Population)
        if paramname is None or 'Events' in paramname  :
            ret.append(self.NumberOfEvents)
        if paramname is None or 'Window' in paramname:
            ret.append(self.WindowSize)
        if paramname is None or 'Slide' in paramname :
            ret.append(self.Slide)
        if paramname is None or 'Dir' in paramname:
            ret.append(self.Dir)

        return ret

    def Norm(self,MyMean, MySTD, Xmin=1):
        rndVal = MySTD * np.random.normal(size=1) + MyMean
        rndVal = max(1, rndVal)
        return int(round(rndVal))

    def DrawPLSample(self,alpha, Xmin, n, mode='Cont', Xmax=None):
        expt = 1.0 / (1.0 - alpha)
        ysamples = np.random.random(n)
        sample = Xmin * ysamples ** expt
        if Xmax is not None:
            sample = sample[sample < Xmax]
        if mode == 'Discrete':
            sample = [float(int(x)) for x in sample]
        sample = [x - 1 for x in sample]
        return sample

    def DrawEXPSample(self,my_lambda, n, mode='Cont', Xmax=None):
        ysamples = np.random.random(n)
        sample = np.log(1 - ysamples) / (-1 * my_lambda)
        if mode == 'Discrete':
            sample = [np.ceil(1 / x) for x in sample]
        else:
            sample = [1 / x for x in sample]
        if Xmax is not None:
            sample = [x for x in sample if x < Xmax]
        sample = [x - 1 for x in sample]
        return sample

    def DrawLogNormalSample(self, n, mu=0, sigma=1, Xmin=0, Xmax=None, mode='Cont'):
        s = np.random.normal(mu, sigma, size=n)
        s = [np.exp(mu + sigma * x) for x in s]
        s = [x for x in s if x >= Xmin]
        if Xmax is not None:
            s = [x for x in s if x <= Xmax]
        if mode == 'Discrete':
            s = [np.ceil(x) for x in s]
        s = [x - 1 for x in s]
        return s

    def GenerateSyntheticData(self,Activation = None):
        if Activation is not None:
            self.Activation = Activation
            self.Dir = Activation['HomeDir'] + '/P_' + str(Activation['Population']) \
                   + "E_" + str(Activation['TotalEvents']) + 'Dist_' + self.Activation['Distributions'][1] \
                       + "DU_" + str(self.Activation['Interval_Duration']['mu']) \
                       + "_" + str(self.Activation['Interval_Duration']['sigma'])

        self.DegreeVector = pd.DataFrame(columns=["Interval", "DegreeVector", 'Model', 'Params',
                                             'CP', 'ActiveActors'])
        self.GroundTruth = pd.DataFrame(columns=["Event", "Interval", "Duration"])
        alpha = self.Activation['Powerlaw']['alpha']
        mu = self.Activation['Lognormal']['mu']
        sigma = self.Activation['Lognormal']['sigma']
        my_lambda = self.Activation['Exponential']['my_lambda']
        intervals_between_events_mu = self.Activation['Interval_Duration']['mu']
        intervals_between_events_sigma = self.Activation['Interval_Duration']['sigma']
        primary_distribution = self.Activation['Distributions'][0]
        secondary_distribution = self.Activation['Distributions'][1]

        interval = 1
        CP = 0
        for ev in range(self.Activation['TotalEvents']):
            next_duration = self.Norm(intervals_between_events_mu, intervals_between_events_sigma)
            self.GroundTruth = self.GroundTruth.append({"Event": ev, "Interval": interval, "Duration": next_duration},
                                   ignore_index=True)
            for i in range(next_duration):
                if CP == 1:
                    DegreeSample = self.DrawPLSample(n=self.Activation['Population'], alpha=alpha, Xmin=1, mode='Discrete')
                    model, params = 'powerlaw', [alpha]
                else:
                    if secondary_distribution == 'Lognormal':
                        DegreeSample = self.DrawLogNormalSample(n=self.Activation['Population'], mu=mu, sigma=sigma,
                                                           mode='Discrete')
                        model, params = 'Lognormal', [mu, sigma]
                    else:
                        DegreeSample = self.DrawEXPSample(n=self.Activation['Population'], my_lambda=my_lambda,
                                                     mode='Discrete')
                        model, params = 'Exponential', [my_lambda]
                ActiveActors = len([x for x in DegreeSample if x > 0])
                self.DegreeVector = self.DegreeVector.append({"Interval": interval,
                                                    "DegreeVector": DegreeSample, "Model": model, "Params": params,
                                                    "CP": CP, 'ActiveActors': ActiveActors}, ignore_index=True)
                interval += 1
            CP = (CP + 1) % 2
        sufix = "Dist_" + secondary_distribution + "Du_" + str(intervals_between_events_mu) +\
                 "_" + str(intervals_between_events_sigma)
        Dir = self.Activation['HomeDir'] +'/P_' + str(self.Activation['Population']) + "E_" + str(self.Activation['TotalEvents']) + sufix
        if not os.path.exists(Dir):
            os.makedirs(Dir)
        with open(Dir + '/Parameters.txt', 'w') as the_file:
            the_file.write('Number of Actors = ' + str(self.Activation['Population']) + '\n')
            the_file.write('Number of Events = ' + str(self.Activation['TotalEvents']) + '\n')
            the_file.write('Number of intervals between events is normally distributed ' +
                           'with the following parameters: mu=' + str(intervals_between_events_mu)
                           + " sigma^2=" + str(intervals_between_events_sigma) + '\n')
            the_file.write('For each event the distribution is flipped between two distributions: ')
            the_file.write('Power-law with hyper-parameters: Alpha= ' + str(alpha) + '\n')
            if secondary_distribution == 'Lognormal':
                the_file.write('Lognormal with hyper-parameters: mu=' + str(mu) + " sigma^2=" + str(sigma) + '\n')
            else:
                the_file.write('Exponential with hyper-parameters: Lambda=' + str(my_lambda) + '\n')
        the_file.close()
        self.DegreeVector.to_csv(Dir + '/DegreeVector_Interval.csv', encoding='utf-8',
                            index=False, mode='w')
        self.DegreeVector.to_pickle(Dir + '/DegreeVector_Interval.pickle')
        self.GroundTruth.to_csv(Dir + '/GroundTruth.csv', encoding='utf-8', index=False, mode='w')
        self.GroundTruth.to_pickle(Dir + '/GroundTruth.pickle')
        self.Events = []
        for i in range(1, len(self.GroundTruth)):
            self.Events.append(self.GroundTruth['Interval'].iloc[i])

    def LoadGroundTruth(self):

        self.GroundTruth = pd.read_pickle(self.Dir + '/GroundTruth.pickle')
        self.Events = []
        for i in range(1, len(self.GroundTruth)):
            self.Events.append(self.GroundTruth['Interval'].iloc[i])
        return self.Events

    def LoadDegreeVector(self):
        self.DegreeVector = pd.read_pickle(self.Dir + '/DegreeVector_Interval.pickle')

    def WinDegree(self,FullVector,Start,window,pop_size,debug=False):
        WinDegreeV = FullVector.loc[(FullVector['Interval'] >= Start) &
                                    (FullVector['Interval'] <= Start + window-1)]
        if len(WinDegreeV) != window:
            win_deg = -1
        else:
            if debug:
                print (WinDegreeV)
            win_deg = [0] * pop_size
            for i in range(len(WinDegreeV)):
                win_deg = [x+y for x,y in zip(win_deg,WinDegreeV['DegreeVector'].iloc[i])]
        #print(sorted(win_deg))
        #return [x for x in win_deg if x > 0]
        return win_deg

    def SampleWithReplacements(self,sample, size):
        return np.random.choice(sample, len(sample))

    def CalcPValue(self, s_1, s_2, reps=1000):
        ks_d_0, ks_p = ks_2samp(s_1, s_2)
        distance = []
        distancep = []
        cumm_d = 0
        for i in range(reps):
            ks_d, ks_p = ks_2samp(s_1, self.SampleWithReplacements(s_1, len(s_1)))
            distance.append(ks_d)
            distancep.append(ks_p)
            if ks_d_0 < ks_d:
                cumm_d += 1
        return (float(cumm_d) / len(distance))

    def CheckWindowSlide(self,degV, threshold ,w, s, pop , ev = None):
        if ev is None:
            ev = self.Events
        self.UndetectedEvents = ev[:]
        self.DetectedEvents = []
        self.GraphComparison = pd.DataFrame(columns=["Interval", "KS-P-Value",'TP','Confidence'])
        ks_res = []
        last_iteration = self.WinDegree(degV, 1, w, pop)
        for iteration in range(1 + s, len(degV), s):
            step = self.WinDegree(degV, iteration, w, pop)
            if step != -1:
                ks_d, ks_p = ks_2samp(last_iteration, step)

                #************************************************
                #confidence = self.CalcPValue(last_iteration,step)
                confidence = -1
                #************************************************
                tp = -1
                if ks_p < threshold:
                    for e in ev:
                        if e>= iteration and e < iteration + s:
                            tp = e
                            self.UndetectedEvents.remove(e)
                            self.DetectedEvents.append(e)
                            break
                self.GraphComparison = self.GraphComparison.append({"Interval": iteration,
                        "KS-P-Value": ks_p,'TP':tp,'Confidence':confidence}, ignore_index=True)
                ks_res.append([ks_p, [iteration, iteration + w - 1],tp])
                last_iteration = step
        return ks_res, self.GraphComparison

    def plotCPD(self,FoundCPD,threshold,PlotParam = {},ground_truth_at_top = True):
        fig, (ax2) = plt.subplots(nrows=1, ncols=1, dpi=80, figsize=(12, 4))
        days,CPdays,CPdaysTN,CPdaysFP,CPdaysFN,CPdaysTP,impressions,impressionsTN,impressionsFP,impressionsFN,impressionsTP = [], [],[],[],[],[],[],[],[],[],[]

        pinsize = 100

        #Draw the ground truth
        days, impressions = [], []
        for ev in sorted(self.DetectedEvents):
            days.append(ev)
            impressions.append(1.05)
            ax2.axvline(ev, color='g', linestyle='--')
        ax2.scatter(days, impressions, color='blue', marker='D', label="Ground Truth")

        days, impressions = [], []
        for ev in sorted(self.UndetectedEvents):
            days.append(ev)
            if ground_truth_at_top:
                impressions.append(1.05)
            else:
                impressions.append(0.0)
            ax2.axvline(ev, color='r', linestyle='--')
        if days != []:
            ax2.scatter(days, impressions, edgecolors='blue', facecolors='none' ,s=pinsize*0.3,marker='D')
            # ax2.scatter(days, impressions, c='red', marker='D')


        # Draw the threshold line
        ax2.plot(range(len(self.DegreeVector)), [1-threshold] * len(self.DegreeVector), color='b' ,linestyle='--' , label= 'Threshold')

        # Draw Results
        for ev in FoundCPD:
            CPdays.append(ev[1][0])
            impressions.append(1 - ev[0])
            if ev[2] ==  -1:
                # Check here if above threshold
                if ev[0] > threshold:
                    #check if False Negative
                    is_TN = True
                    for undetect in self.UndetectedEvents:
                        if ev[1][0] <= undetect and ev[1][1] >= undetect:
                            is_TN = False
                            CPdaysFN.append(ev[1][0])
                            impressionsFN.append(1 - ev[0])
                    # True Negative
                    if is_TN:
                        CPdaysTN.append(ev[1][0])
                        impressionsTN.append(1 - ev[0])
                else:
                    # False Positive
                    CPdaysFP.append(ev[1][0])
                    impressionsFP.append(1 - ev[0])
            else:
                # True Positive
                CPdaysTP.append(ev[1][0])
                impressionsTP.append(1 - ev[0])
        #    True Positive Results
        if len(CPdaysTP) >0:
            ax2.scatter(CPdaysTP , impressionsTP, c='green',marker = '*', s=pinsize*2,label="True Positive")

        if len(CPdaysTN) > 0:
            rgba = []
            for nn in range(len(CPdaysTN)):
                rgba.append( [0.0,0.7,0.0,0.5])
            ax2.scatter(CPdaysTN, impressionsTN, edgecolors=rgba,facecolors='none',marker = 'o', s=pinsize,label="True Negative")

        if len(CPdaysFP) > 0:
            rgba = []
            for nn in range(len(CPdaysFP)):
                rgba.append( [0.7,0.0,0.0,0.5])
            ax2.scatter(CPdaysFP, impressionsFP, edgecolors=rgba,marker = 'o', s=pinsize, facecolors='none',label="False Positive")

        if len(CPdaysFN) > 0:
            rgba = []
            for nn in range(len(CPdaysFN)):
                rgba.append( [0.7,0.0,0.0,0.5])
            ax2.scatter(CPdaysFN, impressionsFN, edgecolors=rgba,marker = 'o', s=pinsize, facecolors='none',label="False Negative")

        if PlotParam['DrawWindow']:
            for cp,imp in zip(CPdays,impressions):
                xwin,wincolor = [], []
                for win in range(self.Activation['WindowSize']):
                    xwin.append(cp + win)
                    wincolor.append('r')
                ax2.plot(xwin, [imp]*len(xwin),color='b')



        plt.ylabel("Confidence Level")
        plt.xlabel("Time Intervals")
        plt.legend(loc='best')
        if self.Activation['PlotResults']['Save']:
            fig.savefig(self.Dir + '/CPD.png')
        plt.show()

    def FindChangePoint(self, Activation = None):
        if Activation is not None:
            self.Activation = Activation
        self.setdir()
        if self.DegreeVector is None:
            self.LoadDegreeVector()
        if self.GroundTruth is None:
            self.LoadGroundTruth()
        w = self.Activation['WindowSize']
        s = self.Activation['Slide']
        threshold = self.Activation['Threshold']
        dir_name = self.Dir + "/Interval" + str(w) + "_" + str(s)
        if not os.path.exists(dir_name):
            os.makedirs(dir_name)
        FoundCPD, WindowsComparison = self.CheckWindowSlide(self.DegreeVector, threshold=threshold,w=w, s=w, pop=self.Activation['Population'])
        WindowsComparison.to_pickle(dir_name + '/GraphComparison_Interval.pickle')
        WindowsComparison.to_csv(dir_name + '/GraphComparison_Interval.csv')

        if self.Activation['PlotResults']['Plot']:
            self.plotCPD(FoundCPD=FoundCPD,threshold=threshold,PlotParam=Activation['PlotResults'])
        tp = len(self.DetectedEvents)
        fp = len([x for x in FoundCPD if x[0] < threshold]) - tp
        fn = len(self.UndetectedEvents)
        tn = len(self.GraphComparison) - tp - fn - fp
        if (tp+fn) == 0:
            recall = 0
        else:
            recall = tp / float(tp+fn)
        if (tp + fp) == 0:
            precision = 0
        else:
            precision = tp / float(tp + fp)
        return FoundCPD , [tp,tn,fp,fn,recall,precision]

    def ScanAll(self,Activation):
        self.Activation = Activation
        self.GenerateSyntheticData()
        CompareWindowsSlides = pd.DataFrame(columns=["Window", "Slide", 'Recall', 'Precision'])
        for w in range(1, int(Activation['Interval_Duration']['mu'] + Activation['Interval_Duration']['sigma'] + 1)):
            for s in range(1, w):
                self.Activation['WindowSize'] = w
                self.Activation['Slide'] = s
                self.Activation['PlotResults'] = {'Plot': False, 'DrawWindow': True, 'Save': True}
                cpd, CM = self.FindChangePoint()
                CompareWindowsSlides = CompareWindowsSlides.append({"Window": w,
                             "Slide": s, 'Recall': CM[4], 'Precision': CM[5]},ignore_index=True)

        CompareWindowsSlides.to_csv(self.Dir + '/FullConfusionMatrix.csv')

    def Generate_Relaxed_Caveman(self,l, k, p):
        Gcav = nx.relaxed_caveman_graph(l, k, p)
        return Gcav

    def Barabasi(self,n, m):
        return nx.barabasi_albert_graph(n, m)

    def ER(self,n, p , directed=False):
        return nx.erdos_renyi_graph(n, p, directed=directed)

    def GenerateModelSyntheticData(self,Activation = None):

        if Activation is not None:
            self.Activation = Activation
        self.setdir()
        if not os.path.exists(self.Dir):
            os.makedirs(self.Dir)
            os.makedirs(self.Dir + "/Graphs")
        self.DegreeVector = pd.DataFrame(columns=["Interval", "DegreeVector", 'Model', 'Params',
                                             'CP', 'ActiveActors'])
        self.GroundTruth = pd.DataFrame(columns=["Event", "Interval", "Duration"])

        intervals_between_events_mu = self.Activation['Interval_Duration']['mu']
        intervals_between_events_sigma = self.Activation['Interval_Duration']['sigma']
        primary_distribution = self.Activation['Models'][1]['Name']
        secondary_distribution = self.Activation['Models'][2]['Name']

        interval = 1
        CP = 0
        for ev in range(self.Activation['TotalEvents']):
            next_duration = self.Norm(intervals_between_events_mu, intervals_between_events_sigma)
            self.GroundTruth = self.GroundTruth.append({"Event": ev, "Interval": interval, "Duration": next_duration},
                                   ignore_index=True)
            for i in range(next_duration):
                if CP == 1:
                    if primary_distribution == 'Caveman':
                        l = self.Activation['Models'][1]['Cliques']  # Number of cliques
                        k = int(self.Activation['Population'] / l)  # Size of cliques
                        p = self.Activation['Models'][1]['Rewire']  # Probabilty of rewiring each edge.
                        next_graph = self.Generate_Relaxed_Caveman(l, k, p)
                        model, params = 'Caveman', [l, k, p]
                    if primary_distribution == 'PA':
                        t = self.Activation['Models'][1]['Talkative']
                        next_graph = self.Barabasi(self.Activation['Population'],t)
                        model, params = 'PA', [t]
                    if primary_distribution == 'ER':
                        p = self.Activation['Models'][1]['HyperParams'][1]
                        next_graph = self.ER(self.Activation['Population'],p)
                        model, params = 'ER', [p]
                else:
                    if secondary_distribution == 'Caveman':
                        l = self.Activation['Models'][2]['Cliques']  # Number of cliques
                        k = int(self.Activation['Population'] / l)  # Size of cliques
                        p = self.Activation['Models'][2]['Rewire']  # Probabilty of rewiring each edge.
                        next_graph = self.Generate_Relaxed_Caveman(l, k, p)
                        model, params = 'Caveman', [l, k, p]
                    if secondary_distribution == 'PA':
                        t = self.Activation['Models'][2]['Talkative']
                        next_graph = self.Barabasi(self.Activation['Population'], t)
                        model, params = 'PA', [t]
                    if secondary_distribution == 'ER':
                        p = self.Activation['Models'][2]['HyperParams'][1]
                        next_graph = self.ER(self.Activation['Population'], p)
                        model, params = 'PA', [p]
                DegreeSample =  sorted([d for n, d in next_graph.degree()], reverse=True)
                #DegreeSample = [d for n, d in next_graph.degree()]

                #ActiveActors = len([x for x in DegreeSample if x > 0])
                ActiveActors = DegreeSample
                nx.write_gml(next_graph, self.Dir + "/Graphs/interval_" + str(interval) + ".gml")
                self.DegreeVector = self.DegreeVector.append({"Interval": interval,
                                                    "DegreeVector": DegreeSample, "Model": model, "Params": params,
                                                    "CP": CP, 'ActiveActors': ActiveActors}, ignore_index=True)
                interval += 1
            CP = (CP + 1) % 2


        with open(self.Dir + '/Parameters.txt', 'w') as the_file:
            the_file.write('Number of Actors :' + str(self.Activation['Population']) + '\n')
            the_file.write('Number of Events = ' + str(self.Activation['TotalEvents']) + '\n')
            the_file.write('Number of intervals between events is normally distributed ' +
                           'with the following parameters: mu=' + str(intervals_between_events_mu)
                           + " sigma^2=" + str(intervals_between_events_sigma) + '\n')
            the_file.write('For each event the generative model is flipped between the two: \n')
            if primary_distribution == 'Caveman' :
                the_file.write(primary_distribution + ' with hyper-parameters: l= ' + str(l) + " k="+str(k)+" p="+str(p)+'\n')
            if primary_distribution == 'PA':
                the_file.write(
                    primary_distribution + ' with hyper-parameters: Talkative= ' + str(self.Activation['Models'][1]['Talkative']) + '\n')
            if primary_distribution == 'ER':
                the_file.write(
                    primary_distribution + ' with hyper-parameters: '+self.Activation['Models'][1]['HyperParams'][0]+" = " + str(
                        self.Activation['Models'][1]['HyperParams'][1]) + '\n')
            if secondary_distribution == 'Caveman':
                the_file.write(secondary_distribution + ' with hyper-parameters: l=' + str(l) + " k="+str(k)+" p="+str(p) + '\n')
            if secondary_distribution == 'PA':
                the_file.write(
                    secondary_distribution + ' with hyper-parameters: Talkative= ' + str(
                        self.Activation['Models'][2]['Talkative']) + '\n')
            if secondary_distribution == 'ER':
                the_file.write(
                    secondary_distribution + ' with hyper-parameters: '+self.Activation['Models'][2]['HyperParams'][0]+" = " + str(
                        self.Activation['Models'][2]['HyperParams'][1]) + '\n')

        the_file.close()
        self.DegreeVector.to_csv(self.Dir + '/DegreeVector_Interval.csv', encoding='utf-8',
                            index=False, mode='w')
        self.DegreeVector.to_pickle(self.Dir + '/DegreeVector_Interval.pickle')
        self.GroundTruth.to_csv(self.Dir + '/GroundTruth.csv', encoding='utf-8', index=False, mode='w')
        self.GroundTruth.to_pickle(self.Dir + '/GroundTruth.pickle')
        self.Events = []
        for i in range(1, len(self.GroundTruth)):
            self.Events.append(self.GroundTruth['Interval'].iloc[i])



